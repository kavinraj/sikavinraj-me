// test server build by node.js

var connect = require('connect');
var serveStatic = require('serve-static');
connect().use(serveStatic(__dirname)).listen(9090, function(){
    console.log('Server running on http://127.0.0.1:9090...');
});