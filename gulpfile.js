var gulp = require('gulp'), // init
    uglify = require('gulp-uglify'), // min - js
    uglifycss = require('gulp-uglifycss'), // min - css
    plumber = require('gulp-plumber'), // plumber - error log
    concat = require('gulp-concat'), // concat - connect all file into one - node 6
    sourcemaps = require('gulp-sourcemaps'), // sourcemaps generater
    autoprefixer = require('gulp-autoprefixer'), // add pre-fix for all browser
    imagemin = require('gulp-imagemin'), // image min
    htmlmin = require('gulp-htmlmin'), // image min
    browserSync = require('browser-sync'), // browser reload
    path = require('path'), // path for module
    gulpif = require('gulp-if'), // if statement for compiler setting
    swPrecache = require('sw-precache'), // precache for service worker 
    critical = require('critical'); // critical fold generator

var src = 'src',
    dest = 'dev'
    environment = 'production';


gulp.task('html', function() {
   return gulp.src(src + '/**/*.html')
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest(dest + '/'))
    .pipe(browserSync.reload({stream: true}));
    console.log('Uglify HTML from MANUAL');
});


gulp.task('css',function(){
  gulp.src(src + '/css/*.css')
  .pipe(sourcemaps.init())
  .pipe(uglifycss({
      "uglyComments": true
  }))
  .pipe(autoprefixer({
     browsers: ['last 2 versions'],
     cascade: false
  }))
  .pipe(plumber())
  .pipe(concat('main.css'))
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest(dest + '/css'))
  .pipe(browserSync.reload({stream: true}));
    console.log('Uglify CSS from MANUAL ');
});


gulp.task('js', function(){
  gulp.src(src + '/js/*.js')
  .pipe(sourcemaps.init())
  .pipe(plumber())
  .pipe(uglify())
  .pipe(concat('other.js'))
  .pipe(sourcemaps.write('.'))
  .pipe(gulp.dest(dest + '/js'))
  .pipe(browserSync.reload({stream: true}));
    console.log('Uglify JS off from MANUAL ');
});


gulp.task('image', () =>
    gulp.src(src + '/images/**/**.**')
        .pipe(imagemin())
        .pipe(gulp.dest(dest + '/images'))
);

gulp.task('critical', function (cb) {
    critical.generate({
        inline: true,
        base: '.',
        src: 'index.html',
        dest: 'dist/index.html',
        minify: true,
        width: 320,
        height: 480,
        timeout: 900000
    });
});


// browser-sync task for starting the server
gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
});


gulp.task('sw', function(callback) {
  swPrecache.write(path.join(dest, 'service-worker.js'), {
    staticFileGlobs: [dest + '/**/*.{js,html,json,css,png,jpg,gif,svg,eot,ttf,woff}'],
    stripPrefix: dest
  }, callback);
});


// gulp.task('watch', ['browser-sync', 'css'], function(){
//   console.log('Browser Sync off MANUAL RELOAD');
//   gulp.watch("src/css/*.css", ['css']);
//   gulp.watch("js/*.js" , ['uglify']);
//   gulp.watch("*.html").on('change', browserSync.reload);
// });


gulp.task('default', ['watch'],function(){
  console.log('gulp watch');
});
