
<!DOCTYPE html>
<html>
<head>
	<title>Image Editor - jQuery</title>	
	<link rel="stylesheet" type="text/css" href="https://creativesdk.github.io/web-image-editor-drag-and-drop/node_modules/bootstrap/dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="https://creativesdk.github.io/web-image-editor-drag-and-drop/style.css">
</head>
<body class="container">

	<div class="row">
		<div class="col-md-12 text-center">
			<h1>Image Editor</h1>
			<h2>Using jQuery</h2>
		</div>
	</div>

	<div class="row">
		<div class="col-md-2">
			<div class="row">
				<div class="col-md-12">
					<button id="edit-image-button" class="btn btn-primary btn-block button-panel">
						<span class="glyphicon glyphicon-pencil pull-left padleft" aria-hidden="true"></span> Edit
					</button>
				</div>			
				<div class="col-md-12">
					<button id="download-image-button" class="btn btn-success btn-block button-panel">
						<span class="glyphicon glyphicon-save pull-left padleft" aria-hidden="true"></span> Download
					</button>
				</div>
				<div class="col-md-12">
					<button id="reset-image-button" class="btn btn-warning btn-block button-panel">
						<span class="glyphicon glyphicon-repeat pull-left padleft" aria-hidden="true"></span> Reset
					</button>
				</div>
				<div class="col-md-12">
					<button id="clear-image-button" class="btn btn-danger btn-block button-panel">
						<span class="glyphicon glyphicon-remove pull-left padleft" aria-hidden="true"></span> Clear
					</button>
				</div>
				
			</div>
		</div>

		<div class="col-md-10">
			<input id="click-upload" type="file">
			<div id="drop-area" >
				<p>Drop an image here!</p>
				<p>(or click to upload)</p>
			</div>
			<img id="editable-image" class="img-responsive">
		</div>

	</div> <!-- end .row -->

	
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.js"></script>
	<script type="text/javascript" src="https://dme0ih8comzn4.cloudfront.net/imaging/v3/editor.js"></script>
	<script type="text/javascript" src="config.js"></script>
	<script type="text/javascript" src="index.js"></script>
</body>
</html>
